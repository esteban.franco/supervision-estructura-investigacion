# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 13:42:20 2020

@author: Estef
"""

def preprocess_AsisClassroomList(path, rango):
    """simple funcion para concatenar las listas de clase encontradas en ASIS
    IN
    path: lugar donde estan los archivos
    rango: python range mostrando la lista de archivos a concatenar: rante(1,50)

    
    OUT
    main_df:  dataframe concatenando todos los archivos anteriores y haciendo la limpieza
    """
    import pandas as pd
    
    first = str(rango[0]) # primer archivo a leer
    file = pd.read_excel('{}/{}.xlsx'.format(path,first))
    # tomando las columnas del primer archivo para generar el archivo principal
    cols = file.columns
    main_df = pd.DataFrame(data=None,columns=cols)
    for n in rango:
        file = pd.read_excel('{}/{}.xlsx'.format(path,str(n)))
        main_df = pd.concat([main_df,file])
    
    main_df = main_df.drop_duplicates(subset=['ID'], keep='first')

    main_df['Nombres'] = main_df['Nombre'].map(
        (lambda string: string.split(',')[1])).map(lambda string: string.title())

    main_df['Apellidos'] = main_df['Nombre'].map(
        (lambda string: string.split(',')[0])).map(lambda string: string.title())
    
    main_df = main_df.drop(columns=['Nombre','Seleccionar','Sistema Calificación', 'Uni'])
    
    main_df = main_df[['ID','Nombres','Apellidos','Programa y Plan', 'Nivel']]
    
    return main_df

def cross_information(year, semestre, check_col,info_col,df1,df_col):
    """ completar alguna base de datos de con información que se tenga en la 
    la base de datos de asis de lista de estudiantes matriculados
    
    INPUT
    year: int, año que se busca, por ej. 2020
    semestre: int, semestre que se cursa, por ej. 2
    check_col: nombre de la columna con datos a cruzar en la lista de
        matriculados en el ASIS
    info_col: str, nombres de las columnas que tienen la información que se va 
            a adjuntar. Por ej. Email, telefono_contacto
    df1: pandas.DataFrame, base de datos de la que se quiere completar la información
    df_col: str, nombre de columna en df con los datos a cruzar.
    
    OUTPUT
    df: pandas.DataFrame, nuevamente la base de datos con la nueva informacion
                suministrada.
    """
    import pandas as pd
    from pandas import Index
    
    path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín (1)'
            r'\coordinacion-invesigaciones-facultad\codigos')
    listas = ['preg','preg1']
    df = df1.copy()
    df[info_col] = ''  # crear la nueva columna donde se almacenara la informacion
    for i, student in enumerate(df[df_col].values):
        
        for lista in listas:
            filename = 'lista matriculados {} {}-{}.xlsx'.format(lista,
                                                                 year,
                                                                 semestre)
            
            matriculados = pd.read_excel('{}/{}'.format(path,filename),
                                         skiprows=1)
            matriculados = matriculados.drop_duplicates(subset=[check_col],
                                                        keep='first')
            if student in matriculados[check_col].values:
                ind = Index(matriculados[check_col]).get_loc(student)
                # print(i,', ',student,', ',ind)
                df.loc[i,info_col] = matriculados.loc[ind,info_col]
                break
            else:
                # print('Not')
                continue  # continuar el ciclo para buscar al estudiante en 
                          # lista preg1
    
    
    return df