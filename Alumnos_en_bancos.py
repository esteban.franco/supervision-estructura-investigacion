# -*- coding: utf-8 -*-
"""
Created on Sat Aug  8 15:45:27 2020

Este escript es creado para chequear que alumnos de un curso estan en un
banco de proyectos o si estan ya en la lista de proyectos escogidos.

@author: Estef
"""
import pandas as pd

from Quickstart import BP_PE_explorer, bank_download

#%% VARIABLES PRINCIPALES
year ='2020'
sem = '2'
filename = 'Metodologia de investigación {}-{}.xlsx'.format(year,sem)

pathfile = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín'
            r' (1)\coordinacion-invesigaciones-facultad\Control de procesos'
            r'\Pregrado\{}-{}\{}'.format(year,sem,filename.split(' ')[0]))

pathtobancos = (r'C:\Users\Estef\Universidad de San Buenaventura Medellín(1)'
               r'\CIFI (Comite de Investigaciones Facultad de Ingenierías)'
               r' - General\TG\Formato banco de proyectos')
bancos = 'Enlaces_API_x_programa.xlsx'

#%% VARIABLES INDIRECTAS, CREADAS O INVOCADAS
curso = pd.read_excel('{}/{}'.format(pathfile,filename))
#%%
# curso.sort_values(by=['Programa y Plan'], inplace=True, ascending=False)
programas = ['AMBIENTAL','MULTIMEDIA','INDUSTRIAL','SONIDO']
# programas =['MULTIMEDIA','INDUSTRIAL','SONIDO']
# programas =['SONIDO']
repositorios = {}
for key in programas: 
    repositorios[key] = {}
    repositorios[key]['PE'] = ''
    repositorios[key]['BP'] = ''

#%% CARGANGO TODOS LOS BANCOS DE PROYECTO

for programa in programas:
    
    repositorios[programa]['BP'], repositorios[programa]['PE'] = bank_download(programa,pathtobancos,bancos)

curso['Base de datos'] = ''

#%% PROGRAMA PRINCIPAL DE BUSQUEDA
for i, codigo in enumerate(curso['ID'].values):
    for programa in programas:
        if programa in curso.loc[i,'Programa y Plan']:
            found = 0
            for k, ID in enumerate(repositorios[programa]['PE']['CÓDIGOS'].values):

                if str(codigo) in str(ID):
                    curso.iloc[i,6] = repositorios[programa]['PE'].iloc[k,2]
                    curso.loc[i,'Base de datos'] ='PE'                 
                    # if '30000032307' in str(ID):
                    #     print(repositorios[programa]['PE'].iloc[k,5])
                    #     print(curso.loc[i,'Base de datos'], i)
                    # Actualizando correo en caso de que no este en el sistema
                    found = 1
                    if curso.loc[i,'Correo Electronico'] != curso.loc[i,'Correo Electronico']:
                        curso.loc[i,'Correo Electronico'] = repositorios[programa]['PE'].iloc[k,5]
                    break
                
            if not found:
                
                for k, ID in enumerate(repositorios[programa]['BP']['CÓDIGOS'].values):
                    if str(codigo) in str(ID):
                        curso.iloc[i,6] = repositorios[programa]['BP'].iloc[k,2]
                        curso.loc[i,'Base de datos'] ='BP'
                        found = 1
                        break
                        # Actualizando correo en caso de que no este en el sistema
                        if curso.loc[i,'Correo Electronico'] != curso.loc[i,'Correo Electronico']:
                            curso.loc[i,'Correo Electronico'] = repositorios[programa]['PE'].iloc[k,5]
                                       
            if not found:
                # print('not found')
                curso.iloc[i,6] = 'None'
                curso.loc[i,'Base de datos'] = 'Ninguna'
                if codigo == 30000025693:
                    print('pasando por Daniela', curso.loc[i,'Base de datos'])
                    
                


#%% STORE FILE

curso.to_excel('{}/{} proyectos.xlsx'.format(pathfile,filename[:-5]),
                index=False)