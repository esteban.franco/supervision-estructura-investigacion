# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 18:23:03 2020
Versión modificada de Quickstart.py de google para obtener las credenciales para 

Para configurar la lectura desde otra cuenta o desde otro computador.
Seguir los pasos de:
https://towardsdatascience.com/how-to-import-google-sheets-data-into-a-pandas-dataframe-using-googles-api-v4-2020-f50e84ea4530

La pagina de activación en la cuenta de google y generación del token es
https://developers.google.com/sheets/api/quickstart/python
@author: Estef
"""



def gsheet_api_check(Cred_name,SCOPES):
    """
    Función para chequear las credenciales de una cuenta de google en particular que
    esta guardada en el archivo json {Cred_name}
    
    Cred_name string, nombre del archivo json. i.e. credentials.json
    SCOPES, por defecto será: ['https://www.googleapis.com/auth/spreadsheets']
    """
    import pickle
    import os.path
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request
    
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                Cred_name, SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds

def pull_sheet_data(Cred_name,SCOPES,SPREADSHEET_ID,RANGE_NAME):
    """Funcion para cargar una googleshet en una lista
    de python"""
    from googleapiclient.discovery import build
    creds = gsheet_api_check(Cred_name,SCOPES)
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    result = sheet.values().get(
        spreadsheetId=SPREADSHEET_ID,
        range=RANGE_NAME).execute()
    values = result.get('values', [])
    
    if not values:
        print('No data found.')
    else:
        rows = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                  range=RANGE_NAME).execute()
        data = rows.get('values')
        print("COMPLETE: Data copied from {} {}".format(RANGE_NAME,Cred_name[:-4]))
        return data
    return

def BP_PE_explorer(codigo, programa, pathtobanks, filename):
    """la Siguiente función encontrará un código de un estudiante en cualquiera
    de los bancos de proyectos de los diferentes programas de la facultad. 
    Si encuentra el código, devolvera el tema de trabajo del estudiante en la 
    columna del banco de proyectos encontrada.
    INPUT
    codigo: int, codigo del estudiante
    programa: str, nombre o codigo del programa curricular 
                    donde se requiere encontrar el estudiante en su banco de
                    proyectos BP o en proyectos escogidos PE
    pathtobanks: str-path, camino al archivo donde se encuentran los codigos para entrar
                    al BP o al PE de dicho programa, asi como su credencial .json
    filename: str, archivo con los codigos BP, PE y el .json
                    
    OUTPUT
    Proyecto: Metadatos del proyecto
    db_name: Nombre de la base de datos donde fue encontrado el alumno compuesto
            por {PE o BP}_{programa}"""
            
    import pandas as pd
    # leyendo el database que guarda los tokens y direcciones de cada uno de los bancos de proyectos
    banks = pd.read_excel('{}/{}'.format(pathtobanks, filename), index_col='index')
    
    # revisando proyectos escogidos
    
    try:
        data = pull_sheet_data(Cred_name = banks.loc['token',programa],
                          SCOPES = ['https://www.googleapis.com/auth/spreadsheets'],
                          SPREADSHEET_ID = banks.loc['Proyectos escogidos',programa],
                          RANGE_NAME = 'Proyectos escogidos')
        # print('Proyectos escogidos del programa Ing. {} descargada \n'
          # .format(programa))
        
        try: 
            proyectos_escogidos = pd.DataFrame(data=data[1:],columns=data[0])
            # print('Buscando en proyectos escogidos')
            for k, ID in enumerate(proyectos_escogidos['CÓDIGOS'].values):
                if str(codigo) in str(ID):
                     Proyecto = proyectos_escogidos.iloc[k,:]
                     db_name = 'PE_{}'.format(programa)
                     return db_name, Proyecto
        except: print('no fue posible convertir lista PE a base de datos\n')
        
    except: print('Permiso desactualizado o no hay archivo proyectos escogidos'
                  ' en programa Ing. {}'.format(programa))

        
    try:
        data1 = pull_sheet_data(Cred_name = banks.loc['token',programa],
                          SCOPES = ['https://www.googleapis.com/auth/spreadsheets'],
                          SPREADSHEET_ID = banks.loc['Banco de Proyectos',programa],
                          RANGE_NAME = 'Banco de Proyectos')
        # print('Banco de Proyectos del programa Ing. {} descargada \n'
          # .format(programa))
        try: 
            # desechando filas vacias y proyectos que no han sido llenados
            Proyectos = [datos[:7] for datos in data1[1:] if len(datos) >= 6
                         and datos[0]!= '']
            # desechando proyectos que no han sido llenados pues no tienen la
            # la celda 'CREADO POR' llena
            # Proyectos = [datos for datos in Proyectos if datos[0] != '']
            # datos_proyectos = [datos[:7] for datos in Proyectos]
            banco_proyectos = pd.DataFrame(data=Proyectos, columns = ['CREADO POR','ACTA CC BP', 
                                        'TITULO PROYECTO O TEMA DE INVESTIGACIÓN',
                                        'DESCRIPCIÓN Y REQUISITOS','NOMBRES Y APELLIDOS',
                                        'CORREOS','CÓDIGOS'])
            # print('Buscando en banco de proyectos')
            # Encontrando la columna de CODIGOS en banco de proyectos
            column = [string for string in banco_proyectos.columns if 'CÓDIGOS' in string]
            for k, ID in enumerate(banco_proyectos[column].values):
                if str(codigo) in str(ID):
                     Proyecto = banco_proyectos.iloc[k,:]
                     db_name = 'BP_{}'.format(programa)
                     return db_name, Proyecto
            
        except:
            print('no fue posible convertir lista BP a base de datos\n')
        
    except: print('Permiso desactualizado o no hay archivo Banco de Proyectos'
                  ' en programa Ing. {}'.format(programa))
    
        
    Proyecto = None
    db_name = 'Ninguno'
    return  db_name, Proyecto  
    
def bank_download(programa,pathtobancos, bancos):
    """Descarga los repositorios PE o BP de la nube de cada programa en dos
    bases de datos tipo pandas DataFrame
    
    INPUT
    programa: str, Nombre de los programs en mayúscula, e.j. 'SONIDO','MULTIMEDIA'...
    pathtobancos: str, camino al repositorio que guarda las direcciones del banco de proyectos.
    bancos: str, nombre del archivo, API_x_programa.xlsx
    
    OUTPUT
    banco_proyectos: pd.DataFrame.
    proyectos_escogidos: pd.DataFrame.
    """
    import pandas as pd
    # leyendo el database que guarda los tokens y direcciones de cada uno de los bancos de proyectos
    banks = pd.read_excel('{}/{}'.format(pathtobancos, bancos), index_col='index')
    
    try:
        data = pull_sheet_data(Cred_name = '{}/{}'.format(pathtobancos,banks.loc['token',programa]),
                          SCOPES = ['https://www.googleapis.com/auth/spreadsheets'],
                          SPREADSHEET_ID = banks.loc['Proyectos escogidos',programa],
                          RANGE_NAME = 'Proyectos escogidos')
        
        try: 
            proyectos_escogidos = pd.DataFrame(data=data[1:],columns=data[0])

        except: 
            print('no fue posible convertir lista PE a base de datos\n')
            proyectos_escogidos = pd.DataFrame()
    except: 
        print('Permiso desactualizado o no hay archivo proyectos escogidos'
                  ' en programa Ing. {}'.format(programa))
        proyectos_escogidos = pd.DataFrame()
            
    try:
        data1 = pull_sheet_data(Cred_name = banks.loc['token',programa],
                          SCOPES = ['https://www.googleapis.com/auth/spreadsheets'],
                          SPREADSHEET_ID = banks.loc['Banco de Proyectos',programa],
                          RANGE_NAME = 'Banco de Proyectos')
        
        try: 
            # desechando filas vacias y proyectos que no han sido llenados
            Proyectos = [datos[:7] for datos in data1[1:] if len(datos) >= 6
                         and datos[0]!= '']
            banco_proyectos = pd.DataFrame(data=Proyectos, columns = ['CREADO POR','ACTA CC BP', 
                                        'TITULO PROYECTO O TEMA DE INVESTIGACIÓN',
                                        'DESCRIPCIÓN Y REQUISITOS','NOMBRES Y APELLIDOS',
                                        'CORREOS','CÓDIGOS'])
            # print('Buscando en banco de proyectos')
            # Encontrando la columna de CODIGOS en banco de proyectos
                        
        except:
            print('no fue posible convertir lista BP a base de datos\n')
            banco_proyectos = pd.DataFrame()
    except: 
        print('Permiso desactualizado o no hay archivo Banco de Proyectos'
                  ' en programa Ing. {}'.format(programa))
        banco_proyectos = pd.DataFrame()
    
    return banco_proyectos, proyectos_escogidos