## Supervision estructura investigacion

Este proyecto esta destinado a generar scripts para vigilar los procesos académicos de investigación muchos de los scripts estarán soportados por funciones que se encuentran en ASI_management.py y en Quickstart.py 

* _ASI_management.py_ funciones auxiliares para preprocesar las listas generadas por el sistema ASIS o para apoyar otras listas generadas por otros scripts y que necesiten soporte del sistema ASIS.
* _Quickstart.py_ funciones base para leer las bases de datos que esten en las nuble de google drive (google sheets).
* *Inscritos_metodologia.py* programa que genera toda la lista de supervisión de los estudiantes de metodología de la Investigación basada en las listas del sistema ASIS.
* *Inscritos_seminario.py* programa que genera toda la lista de supervisión de los estudiantes del curso Seminario de investigación basada en las listas del sistema ASIS.
* *Inscritos_TDG.py* programa que genera toda la lista de supervisión de los estudiantes del curso Trabajo de grado basada en las listas del sistema ASIS.
* *Supervision_TDG.py* Programa para supervisar que los estudiantes que llegan a proceso de TDG hayan cumplido con la estructura académica de investigación de la facultad.


