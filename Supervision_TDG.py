# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 19:52:53 2020

@author: Estef
"""
import logging
import pandas as pd
from pandas import Index
import os
from datetime import datetime
from Quickstart import bank_download
#%% VARIABLES PRINCIPALES

year = 2020
sem = 2
semestres = ['2020-1', '2019-2','2019-1'] # semestres de chequeo en seminario
path_list_est_tdg = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura '
                     r'Medellín (1)\coordinacion-invesigaciones-facultad\CIFI\TG\2020-2'
                     r'\Estudiantes inscritos')
main_path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín (1)'
        r'\coordinacion-invesigaciones-facultad\codigos\supervision-estructura-investigacion')

#%% VARIABLES SECUNDARIAS

lista_est_tdg = 'Trabajos de grado {}-{}.xlsx'.format(year,sem)
# ------ logging configuration ---------------

logfile= 'Supervision TDG {}-{}.log'.format(year, sem)

# adding handler for every messages
fh = logging.FileHandler('{}/{}'.format(main_path,logfile))
fh.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
loggercheck = logging.getLogger('check')
loggercheck.setLevel(logging.INFO)
loggercheck.addHandler(fh)

# adding handler for titles
ftitle = logging.FileHandler('{}/{}'.format(main_path,logfile))
ftitle.setLevel(logging.INFO)
formattitle = logging.Formatter('%(message)s')
ftitle.setFormatter(formattitle)
loggermain = logging.getLogger('main')
loggermain.setLevel(logging.INFO)
loggermain.addHandler(ftitle)

#%% CARGANDO BASE DE DATOS PRINCIPAL (tdg)
loggermain.info('SUPERVISION ESTUDIANTES EN TDG PERIODO {}-{} \n'.format(year,sem))   
est_en_TDG = pd.read_excel('{}/{}'.format(path_list_est_tdg,lista_est_tdg))
est_en_TDG['Tema de investigación encontrado'] = ' '
est_en_TDG['Base de datos'] = ' '
loggermain.info('Base de datos {} cargada'.format(lista_est_tdg))
loggermain.info('\n ----------------------------------------------------------\n')

#%% descargando bancos de proyectos x programas
pathtobancos = (r'C:\Users\Estef\Universidad de San Buenaventura Medellín(1)'
               r'\CIFI (Comite de Investigaciones Facultad de Ingenierías)'
               r' - General\TG\Formato banco de proyectos')
bancos = 'Enlaces_API_x_programa.xlsx'


programas = ['AMBIENTAL','MULTIMEDIA','INDUSTRIAL','SONIDO']

repositorios = {}
for key in programas: 
    repositorios[key] = {}
    repositorios[key]['PE'] = ''
    repositorios[key]['BP'] = ''
    
loggermain.info('Descargando respostorios de proyectos por programa \n')
for programa in programas:
    
    repositorios[programa]['BP'], repositorios[programa]['PE'] = bank_download(programa,pathtobancos,bancos)

# # ------ nombre de bases de datos a chequear
# proy_esco_sonido = '1psheKx3Ck_erw8xKtJ0TgmoBY7MRVzdcn089JgF7kGg'
# banc_proy_sonido = '    '


ctime = datetime.now()
loggermain.info('Fecha de supervisión: {}\n'\
                     .format(ctime.strftime('%Y-%m-%d %H:%M:%S')))


#%% ---- listas de seminario -----------------

seminarios = {} # Diccionario para guardar las bases de datos de seminarios
loggercheck.info('Cargando listas de seminarios en los periodos {} \n'.format(semestres))

for periodo in semestres:
    seminarios_path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura'
                       r' Medellín (1)\coordinacion-invesigaciones-facultad'
                       r'\Control de procesos\Pregrado\{}\Seminario').format(periodo)
    if os.path.exists(seminarios_path):
        try:
            file = '{}/Seminario de investigación {}.xlsx'.format(seminarios_path,
                                                                periodo)
            seminarios[periodo] = pd.read_excel(file)
            loggercheck.info('Abriendo archivo seminario de investigación {}'.format(periodo))
            est_en_TDG['Seminario '+periodo] = ''
            est_en_TDG['Sem '+ periodo +' Exitoso'] = ''
        except:
            loggercheck.warning('No hay archivo Seminario de investigacion'\
                                ' {}'.format(periodo))
            seminarios[periodo] = False
    else:
        loggercheck.warning('El camino al semestre {} no existe'.format(periodo))
        seminarios[periodo] = False

#%% Empezando el proceso de chequeo en bancos 

loggermain.info('\n Empezando proceso de chequeo x Estudiante\n')
loggermain.info('--------------------------------------------------------\n')

loggermain.info('Buscando  en los registros de proyectos del programa PE y BP \n')
for ind, student in enumerate(est_en_TDG['ID']):
    for programa in programas:
        
        # buscando el programa al que pertenece el estudiante
        if programa in est_en_TDG.loc[ind,'Programa y Plan']:
            
            for k, codigo in enumerate(repositorios[programa]['PE']['CÓDIGOS'].values):
                if str(student) in str(codigo):
                    est_en_TDG.loc[ind,'Tema de investigación encontrado'] = \
                        repositorios[programa]['PE'].loc[k,'TITULO PROYECTO O TEMA DE INVESTIGACIÓN']
                    est_en_TDG.loc[ind,'Base de datos'] = 'PE '+ programa
                    break
            
            for k, codigo in enumerate(repositorios[programa]['BP']['CÓDIGOS'].values):
                if str(student) in str(codigo):
                    est_en_TDG.loc[ind,'Tema de investigación encontrado'] = \
                        repositorios[programa]['BP'].loc[k,'TITULO PROYECTO O TEMA DE INVESTIGACIÓN']
                    est_en_TDG.loc[ind,'Base de datos'] = 'BP '+ programa
                    break

#%% Proceso de chequeo en seminario
   
loggermain.info('\n Empezando proceso de busqueda en Seminarios\n')
for ind, student in enumerate(est_en_TDG['ID']):
    for periodo in seminarios.keys():
        if isinstance(seminarios[periodo],pd.DataFrame):
            try:
                k = Index(seminarios[periodo]['ID']).get_loc(student)
                est_en_TDG.loc[ind,'Seminario '+periodo] =\
                    seminarios[periodo].loc[k,'Tema en Banco de proyectos']
                sem_check = 'Sem {} Exitoso'.format(periodo)
                est_en_TDG.loc[ind,sem_check] = \
                    seminarios[periodo].loc[k,'Finalización exitosa']
            except:
                continue
            

#%% almacenar lista

now = datetime.now().date()
est_en_TDG.to_excel('{}_analizada.xlsx'.format(lista_est_tdg[:-5]),
                    sheet_name='Actualizado al {}'.format(str(now)),
                    index=False)