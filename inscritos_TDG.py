# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 12:54:25 2020

@author: Estef
"""
from asis_management import preprocess_AsisClassroomList
from datetime import datetime

#%%
year = 2020
sem = 2
path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín (1)'
        r'\coordinacion-invesigaciones-facultad\CIFI\TG\2020-2\Estudiantes inscritos')
rango = range(12,53)   

#%% 
name_curso = 'Trabajos de grado {}-{}.xlsx'.format(year,sem)  
now = datetime.now().date()   

main_df = preprocess_AsisClassroomList(path, rango) 
 
main_df = main_df.drop(columns=['Nivel'])

main_df['Asesor'] = ''
main_df['Titulo trabajo de grado']

main_df.to_excel('{}/{}'.format(path,name_curso),index=False,
                  sheet_name = 'Actualizada al '+ str(now))
    