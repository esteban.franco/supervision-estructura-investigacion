# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 14:13:41 2020

@author: Estef
"""

from ASIS_management import preprocess_AsisClassroomList, cross_information
from datetime import datetime
import pandas as pd
year = 2020
sem = 2
now = datetime.now().date()
path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín (1)'
        r'\coordinacion-invesigaciones-facultad\Control de procesos\Pregrado'
        r'\{}-{}\Metodologia'.format(year,sem))
#%%
rango = range(1,5)
name_curso = 'Metodologia de investigación {}-{}.xlsx'.format(year,sem)
main_df =preprocess_AsisClassroomList(path,rango)

main_df['Profesor del curso'] = 'Andres Mauricio Cardenas'
main_df['Tema en Banco de proyectos']=''


main_df.to_excel('{}/{}'.format(path,name_curso), index=False,
                 sheet_name = 'Actualizado al '+str(now))

#%%  leyendo otra vez base de datos para evadir repetición de indices

main_df = pd.read_excel('{}/{}'.format(path, name_curso))

#%%
final_df =cross_information(year, sem,
                  'ID Estud','Correo Electronico',
                  main_df,'ID')

final_df.to_excel('{}/{}'.format(path,name_curso), index=False,
                  sheet_name = 'Actualizado al '+str(now))

