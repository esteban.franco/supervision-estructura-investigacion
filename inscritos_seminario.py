# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 11:13:54 2020

Programa para arreglar generar toda la lista de estudiantes de seminario.

@author: Estef
"""
from ASIS_management import preprocess_AsisClassroomList, cross_information
from datetime import datetime
import pandas as pd
year = 2020
sem = 2
path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín (1)'
        r'\coordinacion-invesigaciones-facultad\Control de procesos\Pregrado'
        r'\{}-{}\Seminario'.format(year,sem))

rango = range(1,6)
name_curso = 'Seminario de investigación {}-{}.xlsx'.format(year,sem)
main_df =preprocess_AsisClassroomList(path,rango)
main_df['Profesor del curso'] = ''
main_df['Tema en Banco de proyectos']=''

now = datetime.now().date()
main_df.to_excel('{}/{}'.format(path,name_curso), index=False,
                 sheet_name = 'Actualizado al '+str(now))


#%%  leyendo otra vez base de datos para evadir repetición de indices

main_df = pd.read_excel('{}/{}'.format(path, name_curso))

#%%
final_df =cross_information(year, sem,
                  'ID Estud','Correo Electronico',
                  main_df,'ID')

final_df.to_excel('{}/{}'.format(path,name_curso), index=False,
                  sheet_name = 'Actualizado al '+str(now))